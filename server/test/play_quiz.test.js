process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const expect = chai.expect;
const assert = chai.assert;
chai.use(chaiHttp);
describe('Play Quiz', () => {
  let token;
  let userName='alicesmith';
  let pass='password123';
  let quizName='testing1';
  let quizPin='12345';
  before(async () =>{
    try{
      const loginRes = await chai.request(server)
      .post('/auth/login')
        .send({
          userName: userName,
          password: pass
        });
        expect(loginRes.body).to.have.property('message').that.equal('Login Successfull!');
      token = "Bearer "+loginRes.body.accesstoken+" "+loginRes.body.refreshtoken;
    }
    catch(error){
      throw(error);
    }
  })


  it('Check Initiate', async () => {
    try {
      const res = await chai.request(server)
        .post('/play_quiz/initiate')
        .set('Authorization', token)
        .send({
          quizName: quizName,
          quizPin: quizPin
        });
        expect(res.body).to.have.property('message').that.equal('Quiz Access Approved!');
    } catch (error) {
      throw error;
    }
  }).timeout(10000);


  it('Check User Info', async () => {
    try {
      const res = await chai.request(server)
        .post('/play_quiz/playQuizLoadData')
        .set('Authorization', token)
        .send({
            quizName:quizName
        })
        expect(res.body).to.have.property('message').that.is.oneOf(["Player of Play Quiz Not Found","Play Quiz Data Found"]);
    } catch (error) {
      throw error;
    }
  }).timeout(10000);

  it('Check Answer', async () => {
    try {
      const res = await chai.request(server)
        .post('/play_quiz/checkAnswer')
        .set('Authorization', token)
        .send({
            quizName:quizName,
            optionValue:"A",
            currentQNo:1
        });
        expect(res.body).to.have.property('message').that.is.oneOf(["Correct answer, Player not found","Incorrect answer"]);
    } catch (error) {
      throw error;
    }
  }).timeout(10000);
  

  it('Check Leaderboard', async()=>{
    try{
        const res = await chai.request(server)
        .post('/play_quiz/leaderboard')
        .set('Authorization', token)
        .send({
            quizName:quizName
        });
        expect(res.body).to.be.an('array');;
    }
    catch(error){
        throw error;
    }
  }).timeout(10000);


});
