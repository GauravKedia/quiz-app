process.env.NODE_ENV = "test";
const http = require("http");
const express = require("express");
const { Server } = require("socket.io");
const chai = require("chai");
const chaiHttp = require("chai-http");
const ioClient = require("socket.io-client");
const startSocket = require("../controller/playQuiz/startSocket.js");
const connectDatabase = require("../connectDatabase");
var database;
if (process.env.NODE_ENV === "test") {
  database = "quizgametesting";
} else {
  database = "quizgame";
}
const url =
  process.env.NODE_ENV === "test"
    ? `mongodb+srv://${process.env.Username}:${process.env.Password}@cluster0.srmrlxz.mongodb.net/${database}`
    : `mongodb+srv://${process.env.Username}:${process.env.Password}@cluster0.vwqvt9o.mongodb.net/${database}`;

connectDatabase(database, url);

chai.use(chaiHttp);

const PORT = 8081;

describe("Socket Events", function () {
  let server, socket, socket2;
  let token, token2, refreshToken, refreshToken2;
  let userName = "alicesmith";
  let userName2 = "bobjohnson";
  let pass = "password123";
  let quizName1 = "testing1";
  let quizName2 = "testing2";
  let quizPin = "12345";
  this.timeout(5000);

  before(async function () {
    const app = express();
    server = http.createServer(app);

    const io = new Server(server, {
      cors: {
        origin: "*",
        methods: ["GET", "POST"],
        credentials: true,
      },
    });

    startSocket(io);

    server.listen(PORT, () => {
      console.log(`Test Server listening on port ${PORT}`);
    });

    token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImJvYmpvaG5zb24iLCJpYXQiOjE3MDkxODM5NzAsImV4cCI6MTcwOTIxMjc3MH0.qqeL4O_zzFHxiYspMozelFaZ2VzSBL7Ysv_2fod7fLo";
    refreshToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImJvYmpvaG5zb24iLCJpYXQiOjE3MDkxODM5NzAsImV4cCI6MTcwOTI0MTU3MH0.bEsFxxQY7WVHkSP7fCtruE_BERmwaBnzUnGn3RYMShY";
    token2 =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImFsaWNlc21pdGgiLCJpYXQiOjE3MDkxODM5NjksImV4cCI6MTcwOTIxMjc2OX0.JrI-1g2s42oK7GVdB46I_cebpMk32Eh7yPIudGpyfSI";
    refreshToken2 =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImFsaWNlc21pdGgiLCJpYXQiOjE3MDkxODM5NjksImV4cCI6MTcwOTI0MTU2OX0.XCohs3OwutU2pSaEpNoaViTSWfF9LFMy7oerxXNQYG8";
    socket = ioClient(`http://localhost:${PORT}`, {
      auth: {
        token: token,
        refreshToken: refreshToken,
      },
    });

    socket.connect();

    await new Promise((resolve) => {
      socket.on("connect", () => {
        resolve();
      });
    });

    socket2 = ioClient(`http://localhost:${PORT}`, {
      auth: {
        token: token2,
        refreshToken: refreshToken2,
      },
    });

    socket2.connect();

    await new Promise((resolve) => {
      socket2.on("connect", () => {
        resolve();
      });
    });
  });

  it("Check Game Started - Test 1 : Quiz Over", function (done) {
    socket.emit("check_game_started", { quizName: quizName1 }, (response) => {
      chai.expect(response).that.is.oneOf(["Quiz Over"]);
      done();
    });
  });

  it("Check Game Started - Test 2 : Quiz Yet to start", function (done) {
    try {
      socket2.emit(
        "check_game_started",
        { quizName: quizName2 },
        (response) => {
          chai.expect(response).to.be.oneOf(["Quiz Over", "Sending Question!"]);
          done();
        }
      );
    } catch (error) {
      done();
    }
  }).timeout(2000);

  it("Request Question - Test 1 :Quiz Over", function (done) {
    try {
      socket.emit("request_question", quizName1, (response) => {
        chai.expect(response).that.is.oneOf(["Question Over!"]);
        done();
      });
    } catch (error) {
      done();
    }
  }).timeout(2000);

  it("Request Question - Test 2 : Receive Question", function (done) {
    try {
      socket2.on("receive_question", (data) => {
        chai.expect(data.quizName).to.equal(quizName2);
        done();
      });
      socket2.emit("request_question", quizName2);
    } catch (error) {
      done();
    }
  });

  it("Emit on_disconnect event after socket disconnect", function (done) {
    socket.emit("on_disconnect", quizName1, (response) => {
      chai
        .expect(response.callback)
        .to.equal("Socket Disconnect and is Live Changed");
      done();
    });
  });
});

// process.env.NODE_ENV = "test";
// const chai = require("chai");
// const chaiHttp = require("chai-http");
// const server = require("../server");
// const expect = chai.expect;

// const ioClient = require("socket.io-client");
// const backendUrl = "http://localhost:8080";

// chai.use(chaiHttp);

// describe("Play Quiz", () => {
//   let socket;
//   let token, refreshToken;
//   let userName = "alicesmith";
//   let pass = "password123";
//   let quizName = "testing";
//   let quizPin = "12345";
//   //   console.log("Hi0")
//   before(async () => {
//     try {
//       console.log("Hi1");
//       const loginRes = await chai.request(server).post("/auth/login").send({
//         userName: userName,
//         password: pass,
//       });
//       expect(loginRes.body)
//       .to.have.property("message")
//       .that.equal("Login Successfull!");
//       token = loginRes.body.accesstoken;
//       refreshToken = loginRes.body.refreshToken;
//       console.log("TK  : " + token);
//     } catch (error) {
//         throw error;
//     }

//     console.log("Hi2")
//     socket = ioClient(backendUrl, {
//       auth: {
//         token: token,
//         refreshToken: refreshToken,
//       },
//       autoConnect: false
//     });
//     socket.connect();
//     await new Promise((resolve) => {
//       console.log("HI3")
//       socket.on("connect", () => {
//         console.log("HI4 ");
//         resolve();
//       });
//     });
//   });

//   it("should handle check_game_started event", function (done) {
//     socket.emit(
//       "check_game_started",
//       { quizName: "exampleQuiz" },
//       (response) => {
//         // Assert that the response is as expected
//         expect(response).to.equal("ExpectedResponse");
//         done();
//       }
//     );
//   });
// });
