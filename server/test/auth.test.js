process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const expect = chai.expect;
const assert = chai.assert;
const sinon = require('sinon'); 
chai.use(chaiHttp);


describe('Authorization', () => {

  // before(() => {
  //   // Stubbing registerUser function
  //   registerUserStub = sinon.stub(database, 'registerUser');
  //   registerUserStub.resolves({ message: 'Registration Successfull!' });

  //   // Stubbing loginUser function
  //   loginUserStub = sinon.stub(database, 'loginUser');
  //   loginUserStub.withArgs({ userName: 'alicesmith', password: 'password123' }).resolves({
  //     message: 'Login Successfull!',
  //     accesstoken: 'dummyAccessToken',
  //     refreshtoken: 'dummyRefreshToken',
  //     result: { /* Mocked user data */ }
  //   });
  //   loginUserStub.rejects({ message: 'Invalid Username or Password' }); // For other cases
  // });

  // after(() => {
  //   registerUserStub.restore();
  //   loginUserStub.restore();
  // });

  const registerTestCases = [
    {
      firstName: 'Alice',
      lastName: 'Smith',
      email: 'alice.smith@example.com',
      password: 'password123',
      userName: 'alicesmith'
    },
    {
      firstName: 'Bob',
      lastName: 'Johnson',
      email: 'bob@gmail.com',
      password: 'password123',
      userName: 'bobjohnson'
    },
    {
      firstName: 'Bob',
      lastName: 'Johnson',
      email: 'bob',
      password: 'pp',
      userName: 'bobjohnson'
    },
    {
      firstName: 'Bob',
      email: 'bob@gmail.com',
      password: 'pp',
      userName: 'bobjohnson'
    },
  ];

  registerTestCases.forEach((testCase, index) => {
    it(`Register a new user - Test Case ${index + 1}`, async () => {
      try {
        const res = await chai.request(server)
          .post('/auth/register')
          .send(testCase);
        expect(res.body.message).that.is.oneOf(["Registration Successfull!","Invalid email format","All fields are required"]);
      } catch (error) {
        throw error;
      }
    }).timeout(10000);
  });


  const loginTestCases = [
    {
      userName: 'alicesmith',
      password: 'password123'
    },
    {
      userName: 'bobjohnson',
      password: 'pass123'
    },
    // {
    //   userName: 'bobjohnson',
    //   password: 'pp'
    // },
    // {
    //   userName: 'bobjohnson',
    //   password: ''
    // },
  ];

  loginTestCases.forEach((testCase, index) => {
    it(`Login a new user - Test Case ${index + 1}`, async () => {
      try {
        const res = await chai.request(server)
          .post('/auth/login')
          .send(testCase);

          expect(res.body).to.have.property('message').that.is.oneOf(["Invalid Username or Password","Login Successfull!"]);
        if (res.body.message==="Login Successfull!") {
          expect(res.body).to.have.property('accesstoken');
          expect(res.body).to.have.property('refreshtoken');
          expect(res.body).to.have.property('result');
          expect(res.body.result).to.have.property('firstName').that.is.a('string');
          expect(res.body.result).to.have.property('lastName').that.is.a('string');
          expect(res.body.result).to.have.property('email').that.is.a('string');
        }
      } catch (error) {
        throw error;
      }
    }).timeout(10000);
  });
  
});


describe('Authentication', () => {
  let token;
  before(async () =>{
    try{
      const loginRes = await chai.request(server)
      .post('/auth/login')
        .send({
          userName: 'alicesmith',
          password: "password123"
        });
        expect(loginRes.body).to.have.property('message').that.equal('Login Successfull!');
      token = "Bearer "+loginRes.body.accesstoken+" "+loginRes.body.refreshtoken;
    }
    catch(error){
      throw(error);
    }
  })

  const authenticationTestCases = [
    {
      quizName: 'testing',
      quizPin: '12345'
    },
    {
      quizName: '',
      quizPin: 't1'
    },
    {
      quizName: 'test1',
      quizPin: ''
    },
    {
      quizName: '',
      quizPin: ''
    },
  ];

  // authenticationTestCases.forEach((testCase,index)=>{
  //   it(`Check User - Test Case ${index+1}`, async () => {
  //     try {
  //       const res = await chai.request(server)
  //         .post('/create_quiz/initiate')
  //         .set('Authorization', `${token}`)
  //         .send(
  //           testCase
  //         );
  //         expect(res.body).to.have.property('message').that.is.oneOf(['Initiate Successfull!', 'Quiz Already Present for this user']);
  //     } catch (error) {
  //       throw error;
  //     }
  //   }).timeout(10000);
  // })
});
