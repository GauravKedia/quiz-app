process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const expect = chai.expect;
const assert = chai.assert;
chai.use(chaiHttp);
describe('Create Quiz', () => {
  let token,token2;
  let userName='alicesmith';
  let userName2 = 'bobjohnson';
  let pass='password123';
  let quizName1='testing1';
  let quizName2='testing2';
  let quizPin='12345';
  before(async () =>{
    try{
      const loginRes = await chai.request(server)
      .post('/auth/login')
        .send({
          userName: userName,
          password: pass
        });
        expect(loginRes.body).to.have.property('message').that.equal('Login Successfull!');
      token = "Bearer "+loginRes.body.accesstoken+" "+loginRes.body.refreshtoken;
    }
    catch(error){
      throw(error);
    }
    try{
      const loginRes = await chai.request(server)
      .post('/auth/login')
        .send({
          userName: userName2,
          password: pass
        });
        expect(loginRes.body).to.have.property('message').that.equal('Login Successfull!');
      token2 = "Bearer "+loginRes.body.accesstoken+" "+loginRes.body.refreshtoken;
    }
    catch(error){
      throw(error);
    }
  })


  it('Check Initiate - 1', async () => {
    try {
      const res = await chai.request(server)
        .post('/create_quiz/initiate')
        .set('Authorization', token)
        .send({
          quizName: quizName1,
          quizPin: quizPin
        });
        expect(res.body).to.have.property('message').that.is.oneOf(['Initiate Successfull!', 'Quiz Already Present for this user']);
    } catch (error) {
      throw error;
    }
  }).timeout(10000);

  it('Check Initiate - 2', async () => {
    try {
      const res = await chai.request(server)
        .post('/create_quiz/initiate')
        .set('Authorization', token2)
        .send({
          quizName: quizName2,
          quizPin: quizPin
        });
        expect(res.body).to.have.property('message').that.is.oneOf(['Initiate Successfull!', 'Quiz Already Present for this user']);
    } catch (error) {
      throw error;
    }
  }).timeout(10000);

  it('Check Fetch General Info', async () => {
    try {
      const res = await chai.request(server)
        .post('/create_quiz/startLaunch')
        .set('Authorization', token);
        expect(res.body).to.have.property('message').that.equal("Create Quiz Launch Successfull!");
        expect(res.body).to.have.property('userName').that.equal(userName);
        expect(res.body).to.have.property('quizName');
    } catch (error) {
      throw error;
    }
  }).timeout(10000);


  
  it('Check Save Quiz - 1', async()=>{
    try{
      const res1 = await chai.request(server)
      .post('/create_quiz/saveQuiz')
      .set('Authorization', token)
      .send({quizData:{
            quizName:quizName1,
            questionValue:"Q1",
            options: ["A", "B", "C", "D"], 
            correctOption:1,
            points:10,
            solveTime:60
          }});
          const res2 = await chai.request(server)
          .post('/create_quiz/saveQuiz')
          .set('Authorization', token)
          .send({quizData:{
            quizName:quizName1,
            questionValue:"Q2",
            options: ["Q", "W", "E", "R"], 
            correctOption:2,
            points:5,
            solveTime:30
          }});
        expect(res1.body).to.have.property("message").that.equal("Question added to quiz successfully!");
        expect(res2.body).to.have.property("message").that.equal("Question added to quiz successfully!");
      }
      catch(error){
        throw error;
      }
    }).timeout(10000);
    
    
    it('Check Save Quiz - 2', async()=>{
      try{
        const res1 = await chai.request(server)
        .post('/create_quiz/saveQuiz')
        .set('Authorization', token2)
        .send({quizData:{
            quizName:quizName2,
            questionValue:"QQ1",
            options: ["A", "B", "C", "D"], 
            correctOption:1,
            points:10,
            solveTime:60
          }});
        const res2 = await chai.request(server)
        .post('/create_quiz/saveQuiz')
        .set('Authorization', token2)
        .send({quizData:{
          quizName:quizName2,
          questionValue:"QQ2",
          options: ["Q", "W", "E", "R"], 
          correctOption:2,
          points:5,
          solveTime:30
        }});
        expect(res1.body).to.have.property("message").that.equal("Question added to quiz successfully!");
        expect(res2.body).to.have.property("message").that.equal("Question added to quiz successfully!");
      }
      catch(error){
        throw error;
      }
    }).timeout(10000);
    
    it('Check Load Quiz Data', async () => {
      try {
        const res = await chai.request(server)
          .post('/create_quiz/loadQuizData')
          .set('Authorization', token)
          .send({
              quizName:quizName1
          });
          expect(res.body).to.have.property('message').that.is.oneOf(["Question not found","Quiz Found"]);
          if (res.body.message === "Quiz Found") {
              expect(res.body).to.have.property('quiz').that.is.an('object');
              expect(res.body).to.have.property('questionCount').that.is.an('number');
          }
      } catch (error) {
        throw error;
      }
    }).timeout(10000);
    
  it('Check Load Quiz', async()=>{
    try{
        const res = await chai.request(server)
        .post('/create_quiz/loadQuiz')
        .set('Authorization', token)
        .send({
            quizName:quizName1,
            questionNumber:1
        });
        expect(res.body).to.have.property("message").that.equal("Load Sucessfull");
        expect(res.body).to.have.property("question").that.is.an('object');
    }
    catch(error){
        throw error;
    }
  }).timeout(10000);

  it('Check Delete Quiz', async()=>{
    try{
        const res = await chai.request(server)
        .post('/create_quiz/deleteQuiz')
        .set('Authorization', token)
        .send({
            quizName:quizName1,
            questionNumber:2,
        });
        expect(res.body).to.have.property("message").that.equal("Delete Sucessfull");
    }
    catch(error){
        throw error;
    }
  }).timeout(10000);


  it('Check Deploy Quiz - Quiz Over', async()=>{
    try{
        const res = await chai.request(server)
        .post('/create_quiz/deployQuiz')
        .set('Authorization', token)
        .send({
            quizName: quizName1,
            scheduleDate : new Date(Date.now() - 4000000)
        });
        expect(res.body).to.have.property("message").that.equal("Quiz Deployed");
    }
    catch(error){
        throw error;
    }
  }).timeout(5000);

  it('Check Deploy Quiz - Quiz Yet to Start', async()=>{
    try{
        const res = await chai.request(server)
        .post('/create_quiz/deployQuiz')
        .set('Authorization', token2)
        .send({
            quizName: quizName2,
            scheduleDate : new Date(Date.now() + 4000000)
        });
        expect(res.body).to.have.property("message").that.equal("Quiz Deployed");
    }
    catch(error){
        throw error;
    }
  }).timeout(5000);


});
