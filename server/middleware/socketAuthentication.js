require("dotenv").config();
const jwt = require("jsonwebtoken");

const socketAuthentication = (socket, next) => {
  console.log("Socket is :: :: " + socket);
  const token = socket.handshake.auth.token;
  console.log("Socket Authentication : " + token);
  if (!token) {
    console.log("Socket No Authentication Token")
    return;
  }
  console.log("Token is :" + token);
  jwt.verify(token, process.env.secretkey, (error, user) => {
    if (error) {
      console.log("Socket Authentication token invalid");
      return;
    } 
    socket.user = user;
    console.log("Socket Auth Stored User :"+socket.user.userName);
    next();
  });
};

module.exports = socketAuthentication;
