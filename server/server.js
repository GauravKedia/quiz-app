
const express = require('express');
const app = express();
const cors = require("cors")

const http = require('http');
const {Server} = require("socket.io");

const authRouter = require("./routes/authRouter");
const createQuizRouter = require("./routes/createQuizRouter")
const playQuizRouter = require("./routes/playQuizRouter");
const connectDatabase = require("./connectDatabase");
require('dotenv').config();

app.use(express.json());
app.use(cors());

const server = http.createServer(app);
const io = new Server(server,{
  cors:{
    origin: "*",
    methods: ["GET", "POST"],
    credentials: true
  }
});

require("./controller/playQuiz/startSocket")(io);

var database;
if (process.env.NODE_ENV === 'test') {
  database = "quizgametesting";
} else {
  database = "quizgame";
}


const url = process.env.NODE_ENV === 'test' ?
  `mongodb+srv://${process.env.Username}:${process.env.Password}@cluster0.srmrlxz.mongodb.net/${database}` :
  `mongodb+srv://${process.env.Username}:${process.env.Password}@cluster0.vwqvt9o.mongodb.net/${database}`;

connectDatabase(database,url);

app.get('/',(req,res)=>{
  res.status(200).json({
    message:"Server Is Running"
  })
})

app.use('/auth',authRouter);
app.use("/create_quiz",createQuizRouter); 
app.use("/play_quiz",playQuizRouter);



server.listen(process.env.ServerPort, ()=>{console.log("Server Connected to port :" + process.env.ServerPort)});

module.exports = server
