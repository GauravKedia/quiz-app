const mongoose = require("mongoose")
const users = require("../../models/users");
const quiz = require("../../models/quiz");


const saveCreateQuiz = async (req, res) => {
    // console.log("Hi");
    try{
        const {
            quizName,
            questionValue,
            options,
            correctOption,
            points,
            solveTime
        } = req.body.quizData;

        // console.log(req.body.quizData);
        let existingQuiz = await quiz.findOne({ quizName: quizName });

        if (!existingQuiz) {
            let userId = await users.findOne({userName:req.body.userName});
            existingQuiz = new quiz({   
                quizName: quizName,
                hostId: userId,
                questionCount: 0,
                onGoingQuestion:0,
                questions: [],
            });
        }

        const newQuestion = {
            qNo: existingQuiz.questionCount + 1,
            qValue: questionValue,
            timeOfQuestion: solveTime,
            points: points,
            options: options.map((option, index) => ({
                optionNo: index + 1,
                optionValue: option,
                correct: index + 1 === correctOption,
            })),
        };

        existingQuiz.questions.push(newQuestion);
        existingQuiz.questionCount += 1;

        await existingQuiz.save();

        res.send({message:"Question added to quiz successfully!"});
    
    }
    catch(error){
        console.log("Error saving Quiz : "+error);
        return;
    }
};
module.exports = {saveCreateQuiz};
