const quizCreator = require("../../models/quizCreator");
const quizzes = require("../../models/quiz");
const players = require("../../models/player");

const deployQuiz = async (req, res) => {
  try {
    // console.log(" Deploy Quiz Name Recieved "+req.body.quizName);
    const quizName = req.body.quizName;

    const searchQuiz = await quizzes.findOne({ quizName });
    // console.log("S : "+searchQuiz._id);
    if (!searchQuiz) {
      return res.send("Quiz not found");
    }
    // console.log("QQQ:"+quizName);
    // console.log("QQQQ :"+req.body.scheduleDate);
    if (!req.body.scheduleDate) {
      req.body.scheduleDate = new Date(Date.now() + 60000);
    }
    const newQuiz = await quizCreator.findOne({ quizId: searchQuiz._id });
    // console.log(newQuiz);
    if (newQuiz) {
      newQuiz.schedule = req.body.scheduleDate;
      newQuiz.isLive = false;
      await newQuiz.save();
      await players.deleteMany({ quizId: searchQuiz._id });
      res.send({ message: "Quiz Deployed" });
    } else {
      res.send({ message: "Quiz Creator Not Found" });
    }
    return;
  } catch (error) {
    console.log("Error Deploying Quiz : " + error);
    return;
  }
};
module.exports = { deployQuiz };
