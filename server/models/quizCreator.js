    const mongoose = require("mongoose");

    const quizCreator = new mongoose.Schema({
        hostId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "users",
        },
        quizId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "quizzes",
        },
        pin: {
            type: String,
        },
        schedule: {
            type: Date,
        },
        isLive:{
            type:Boolean,
            default:false,
        }
    });

    module.exports = mongoose.model("quizcreators",quizCreator);
