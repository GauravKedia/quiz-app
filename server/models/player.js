const mongoose = require("mongoose")

const players = mongoose.Schema({
    playerId:{
        type:mongoose.Schema.ObjectId,
        ref:"users"
    },
    quizId:{
        type:mongoose.Schema.ObjectId,
        ref:"quizzes"
    },
    score:{
        type:Number,
        default:0
    }
});

module.exports = mongoose.model("players", players);
