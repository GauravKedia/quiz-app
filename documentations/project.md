# Quiz Game Website Documentation

## Project Overview

The Quiz Game Website is an interactive platform designed to host quiz games, allowing users to create and join quizzes, participate in quiz games, and compete with others.

## Project Requirements

### 1. Landing Page

- Provide options for users to create a new game or join an existing game.
- Redirect users to the login page for game creation.
- Enable user registration for new users.

### 2. User Authentication

- Implement user authentication using username and password.
- Allow new users to register and existing users to login.
- Store user information in the database.

### 3. Quiz Creation

- Enable authenticated users to create new quizzes.
- Allow users to input quiz details such as quiz name, unique quiz pin, and quiz questions with options, time limit, and points.
- Implement save and delete options for quiz questions.

### 4. Quiz Management

- Provide options to set the schedule for the quiz.
- Launch the quiz upon setup completion.

### 5. Quiz Gameplay

- Allow players to join quizzes by entering the quiz name and pin.
- Provide a quiz playing interface displaying questions, options, countdown timer, and current score.
- Enable players to submit answers and update scores accordingly.

### 6. Leaderboard

- Display a leaderboard showing usernames and scores of all players.
- Sort the leaderboard in descending order of scores.

## Features

### By Page

|       Page        |      Public      | Authenticated User | Authorized User |
| :---------------: | :--------------: | :----------------: | :-------------: |
|   Landing Page    |  Create/Join Game|   Redirect to Login Page  | - |
|     Login Page    |  Username/Password Fields, Register Option | - | - |
|     Register Page |  User Registration Form  | - | - |
|  Create Quiz Game Page | Quiz Name/Quiz Pin Input, Question Creation, Schedule Setup, Launch Option  | - | Navbar with Username, Question Management, Add Quiz Option |
|  Play Quiz Page   |  Quiz Name/Quiz Pin Input, Quiz Interface with Questions, Options, Timer, Score Display | Navbar with Username, Current Score | - |
|  Leaderboard Page |  Leaderboard with Usernames and Scores  | Navbar with Username | - |

### By Component/Feature

|  Component/Feature  |      Public      | Authenticated User | Authorized User |
| :-----------------: | :--------------: | :----------------: | :-------------: |
|  Quiz Creation Form | -                | Create Quiz, Question Management, Schedule Setup, Launch Quiz | Edit, Delete, Add Quiz |
|  Quiz Interface     | -                | Submit Answer, Update Score                  | Submit Answer, Update Score |
|  Leaderboard        | -                | View Scores           | View Scores |

## Tech Stack

### Frontend

- React.js
- JavaScript

### Backend

- Node.js
- Express.js
- Socket.io

### Testing

- Mocha & Chai

### Database

- MongoDB

### Authentication

- Username/Password Authentication using JWT

### Cloud & Hosting

- Amazon Web Services (AWS)
- Hosting: Render, GitLab
