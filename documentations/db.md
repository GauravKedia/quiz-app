
# Database Schema
## users

Collection to hold user information.

|   \_id   |  firstName  |  lastName  |  userName  |  email  |  password  |
| :------: | :---------: | :--------: | :--------: | :-----: | :--------: |
| ObjectID |   string    |   string   |   string   | string  |   string   |


## quizzes

Collection to hold quiz information.

|   \_id   |  quizName  |  hostId  |  questionCount  |  onGoingQuestion  |  questions  |
| :------: | :--------: | :------: | :-------------: | :---------------: | :---------: |
| ObjectID |   string   | ObjectID (`users`) |     number      |      number       |   subtable  |

### Sub-table: questions

|  \_id  |  qNo  |  qValue  |  timeOfQuestion  |  points  |  options  |
| :----: | :---: | :------: | :--------------: | :------: | :-------: |
| ObjectID| number |  string  |     number       |  number  |  subtable (`options`) |

#### Sub-table: options

|  \_id  |  optionNo  |  optionValue  |  correct  |
| :----: | :--------: | :-----------: | :-------: |
| ObjectID|   number   |     string    | boolean   |


## quizcreators

Collection to hold quiz creator information.

|   \_id   |  hostId  |  quizId  |  pin  |  schedule  |  isLive  |
| :------: | :------: | :------: | :---: | :--------: | :------: |
| ObjectID | ObjectID (`users`) | ObjectID (`quizzes`) | string|    Date    | boolean  |


## players

Collection to hold player information.

|   \_id   |  playerId  |  quizId  |  score  |
| :------: | :--------: | :------: | :-----: |
| ObjectID | ObjectID (`users`)   | ObjectID (`quizzes`) | number  |
