import React, { createContext, useState, useContext } from 'react';

const LoaderContext = createContext();

export const useLoader = () => useContext(LoaderContext);

export const LoaderProvider = ({ children }) => {
  const [universalLoader, setUniversalLoader] = useState(true);

  return (
    <LoaderContext.Provider value={{ universalLoader, setUniversalLoader }}>
      {children}
    </LoaderContext.Provider>
  );
};
