import React, { useEffect, useState } from "react";
import "./CreateQuiz.css";
import NavScrollExample from "./Navbar/CreateQuizNavbar";
import CreateQuizMainArea from "./QuizArea/CreateQuizMainArea";

// console.log("Creating Quiz JS File");

const CreateQuiz = () => {
  const backround_img = process.env.PUBLIC_URL + "/images/background.jpg";
  const [passQuizName, setPassQuizName] = useState("");
  // console.log("Value of passQuizName:", passQuizName);
  return (
    <>
        <div
          className="create_quiz_container"
          style={{ backgroundImage: `url(${backround_img})` }}
        >
          <div className="create_quiz_navbar">
            <NavScrollExample setPassQuizName={setPassQuizName}/>
          </div>
          <div className="create_quiz_inner_container">
            <div className="create_quiz_main_area">
              <CreateQuizMainArea passQuizName={passQuizName} />
            </div>
          </div>
        </div>
    </>
  );
};

export default CreateQuiz;
