import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
import "react-datepicker/dist/react-datepicker.css";
import { useNavigate } from "react-router-dom";
import "./CreateQuizNavbar.css";
import { deployQuiz } from "../../../controller/createQuiz/deployQuiz";
import { createQuizStartLaunch } from "../../../controller/createQuiz/createQuizStartLaunch";
// export const quizNameContext = React.createContext();

const NavScrollExample = ({ setPassQuizName }) => {
  const [userName, setUserName] = useState("");
  const [quizName, setQuizName] = useState("");
  const [scheduledDate, setScheduledDate] = useState(null);
  const currentDate = new Date();
  let navigate = useNavigate();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const createQuizStartLaunchResponse = await createQuizStartLaunch();
        // setUniversalLoader(false);
        setUserName(createQuizStartLaunchResponse.data.userName);
        setQuizName(createQuizStartLaunchResponse.data.quizName);
        setPassQuizName(createQuizStartLaunchResponse.data.quizName);
        // console.log("Nav: " + quizName);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  const handleDateChange = (date) => {
    setScheduledDate(date);
  };

  const deploy_quiz = async (event) => {
    event.preventDefault();
    // setUniversalLoader(true);
    if (!scheduledDate) {
      setScheduledDate(new Date());
    }
    await deployQuiz(quizName, scheduledDate);
    // setUniversalLoader(false);
    let path = `/`;
    navigate(path);
  };

  return (
    // <quizNameContext.Provider value={quizName}>
    <div className="create_quiz_navbar_container">
      <div className="create_quiz_navbar_userName">
        <p>Username : {userName}</p>
      </div>
      <div className="create_quiz_navbar_title">
        <p>Quiz Title : {quizName}</p>
      </div>
      <div className="create_quiz_navbar_schedule">
        <div className="create_quiz_navbar_schedule">
          <DatePicker
            id="scheduleQuiz"
            selected={scheduledDate}
            onChange={handleDateChange}
            minDate={currentDate}
            showTimeSelect
            dateFormat="Pp"
            placeholderText="Schedule Quiz"
            className="create_quiz_navbar_option"
          />
        </div>
      </div>
      <div className="create_quiz_navbar_launch">
        <form onSubmit={deploy_quiz} className="create_quiz_launch_quiz">
          <button className="launch_quiz" type="submit">
            Launch Quiz
          </button>
        </form>
      </div>
    </div>
    // </quizNameContext.Provider>
  );
};

export default NavScrollExample;
