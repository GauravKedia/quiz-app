import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useLoader } from "../../LoaderContext";
import "./LandingPage.css";
import Loader from "../loader/loader";

const create_quiz = process.env.PUBLIC_URL + "/images/create_quiz.png";
const join_quiz = process.env.PUBLIC_URL + "/images/join_quiz.png";
const backround_img = process.env.PUBLIC_URL + "/images/background.jpg";
// console.log("Loading Landing");
const LandingPage = () => {
  const { universalLoader, setUniversalLoader } = useLoader();
  useEffect(() => {
    setUniversalLoader(false);
  }, []);

  const handleLandingPageClick = () => {
    setUniversalLoader(true);
  };

  return (
    <>
      {universalLoader ? (
        <Loader />
      ) : (
        <>
          <div
            className="landing_page"
          >
            <div className="landing_page_nav">
              <div className="landing_page_heading">Quizify</div>
              <div className="landing_page_quiz_buttons">
                  <Link
                    to="/create_quiz/initiate"
                    className="landing_page_quiz_buttons_create"
                    onClick={handleLandingPageClick}
                  >
                    <img src={create_quiz} alt="Create Quiz Image" />
                    Create Quiz
                  </Link>
                  <Link
                    to="/play_quiz/initiate"
                    className="landing_page_quiz_buttons_join"
                    onClick={handleLandingPageClick}
                  >
                    <img src={join_quiz} alt="Create Quiz Image" />
                    Join Quiz
                  </Link>
              </div>
            </div>
            <div className="landing_page_main_area">
              <div className="landing_page_info">
                <h1>Quiz Website</h1>
                <p>Welcome to this quiz game</p>
                <p>Here You can create or play a game</p>
              </div>
              
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default LandingPage;
