import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { initiatePlayQuiz } from "../../controller/PlayQuiz/InitiatePlayQuiz";
import "./InitiatePlayQuiz.css";
import { socket, updateSocketHeaders } from "../../socket";
import initiateSocket from "../../controller/PlayQuiz/initiateSocket";
import { useLoader } from "../../LoaderContext";
import Loader from "../loader/loader";

// console.log("Loading Join Quiz");

const InitiatePlayQuiz = ({ quizName, setQuizName }) => {
  // const [quizName, setQuizName] = useState("");
  const [quizPin, setQuizPin] = useState("");
  const { universalLoader, setUniversalLoader } = useLoader();
  const navigate = useNavigate();

  useEffect(() => {
    setUniversalLoader(false);
  }, []);

  const handleQuizNameChange = (event) => {
    setQuizName(event.target.value);
  };

  const handleQuizPinChange = (event) => {
    setQuizPin(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setUniversalLoader(true);
    try {
      const scheduleResponse = await initiatePlayQuiz(quizName, quizPin);
      if (scheduleResponse) {
        if (scheduleResponse === "Quiz Access Approved!") {
          await updateSocketHeaders();
          // console.log("Socket Before COnnect");
          socket.connect();
          // console.log("Socket After Connect : "+socket.id);
          initiateSocket(quizName, navigate);
        } else if (scheduleResponse === "Please Login Again") {
          window.alert(scheduleResponse);
          setUniversalLoader(false);
          navigate("/auth/login");
        }
        else{
          window.alert(scheduleResponse);
          setUniversalLoader(false);
        }
      } else {
        navigate("/");
      }
    } catch (error) {
      console.log("Error Occured :" + error);
    }
  };

  return (
    <>
      {universalLoader ? (
        <Loader />
      ) : (
        <div className="join_quiz_container">
          <div className="join_quiz_inner_container">
            <h2>Provide Joining Detail</h2>
            <form onSubmit={handleSubmit} className="join_quiz_form_container">
              <input
                name="quiz_name"
                placeholder="Quiz Name"
                autoComplete="off"
                id="quiz_name"
                className="join_quiz_input_container"
                value={quizName}
                onChange={handleQuizNameChange}
              />
              <input
                name="quiz_id"
                placeholder="Quiz PIN"
                autoComplete="off"
                id="quiz_pin"
                className="join_quiz_input_container"
                value={quizPin}
                onChange={handleQuizPinChange}
              />
              <button className="join_quiz_button_container" type="submit">
                Submit
              </button>
            </form>
          </div>
        </div>
      )}
    </>
  );
};

export default InitiatePlayQuiz;
