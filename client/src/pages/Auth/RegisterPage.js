import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Register } from "../../controller/Auth";
import Loader from "../loader/loader";
import { useLoader } from "../../LoaderContext";
import "./RegisterPage.css";

const RegisterPage = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { universalLoader, setUniversalLoader } = useLoader();
  const navigate = useNavigate();

  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleUserNameChange = (event) => {
    setUserName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setUniversalLoader(true);
    try {
      const res = await Register(
        firstName,
        lastName,
        userName,
        email,
        password
      );
      window.alert(res);
      if (res === "Registration Successfull!") {
        navigate("/");
      } else {
        setUniversalLoader(false);
      }
    } catch (error) {
      console.log("Error Registering : " + error);
    }
  };

  useEffect(() => {
    setUniversalLoader(false);
  }, []);
  return (
    <>
      {universalLoader ? (
        <Loader />
      ) : (
        <div className="register_page_container">
          <div className="register_page">
            <h2>Register</h2>
            <form onSubmit={handleSubmit}>
              <div className="register_form_group">
                <label htmlFor="firstName" className="register_label">
                  First Name
                </label>
                <input
                  type="text"
                  className="register_form_control"
                  id="firstName"
                  placeholder="Enter first name"
                  value={firstName}
                  onChange={handleFirstNameChange}
                />
              </div>

              <div className="register_form_group">
                <label htmlFor="lastName" className="register_label">
                  Last Name
                </label>
                <input
                  type="text"
                  className="register_form_control"
                  id="lastName"
                  placeholder="Enter last name"
                  value={lastName}
                  onChange={handleLastNameChange}
                />
              </div>

              <div className="register_form_group">
                <label htmlFor="userName" className="register_label">
                  UserName
                </label>
                <input
                  type="text"
                  className="register_form_control"
                  id="userName"
                  placeholder="Enter userName"
                  value={userName}
                  onChange={handleUserNameChange}
                />
              </div>

              <div className="register_form_group">
                <label htmlFor="email" className="register_label">
                  Email
                </label>
                <input
                  type="email"
                  className="register_form_control"
                  id="email"
                  placeholder="Enter email"
                  value={email}
                  onChange={handleEmailChange}
                />
              </div>

              <div className="register_form_group">
                <label htmlFor="password" className="register_label">
                  Password
                </label>
                <input
                  type="password"
                  className="register_form_control"
                  id="password"
                  placeholder="Enter password"
                  value={password}
                  onChange={handlePasswordChange}
                />
              </div>

              <button type="submit" className="btn btn-primary register_button">
                Register
              </button>
            </form>

            <p className="register_p">
              Already have an account? <Link to="/">Login</Link>
            </p>
          </div>
        </div>
      )}
    </>
  );
};

export default RegisterPage;
