import React, { useState, useEffect } from "react";
// import Profiles from "./profiles";
import "./Leaderboard.css";
import { useNavigate } from "react-router-dom";
import { leaderboard } from "../../controller/PlayQuiz/leaderboard";
import Loader from "../loader/loader";
import { useLoader } from "../../LoaderContext";

const Leaderboard = ({ quizName }) => {
  const [leaderboardData, setLeaderboardData] = useState([]);
  const { universalLoader, setUniversalLoader } = useLoader();
  // let quizName = localStorage.getItem("quizName");
  let navigate = useNavigate();

  useEffect(() => {
    // quizName = localStorage.getItem("quizName");
    // console.log("Leaderboard Page: QuizName " + quizName);
    const fetchLeaderboardData = async () => {
      try {
        const data = await leaderboard(quizName);
        setUniversalLoader(false);
        // console.log(data);
        if (data) {
          // console.log("LeaderBoard data from backend : " + data);
          setLeaderboardData(data);
        }
      } catch (error) {
        console.error("Error fetching leaderboard data: ", error);
      }
    };

    fetchLeaderboardData();
  }, []);

  const return_back = () => {
    setUniversalLoader(true);
    navigate("/");
  };

  return (
    <>
      {universalLoader ? (
        <Loader />
      ) : (
        <div className="leaderboard_container">
          <div className="leaderboard_inner_container">
            <div className="leaderboard_navbar">
              <h1>LeaderBoard</h1>
              <button
                onClick={return_back}
                className="leaderboard_close_button"
              >
                Close
              </button>
            </div>
            <div className="leaderboard_data_container">
              {leaderboardData && leaderboardData.length > 0 ? (
                <table className="leaderboard_table">
                  <tbody>
                    {leaderboardData.map((value, index) => (
                      <tr
                        key={index}
                        className={
                          index === 0
                            ? "leaderboard_row "
                            : "leaderboard_row"
                        }
                      >
                        <td className="leaderboard_number">{index + 1}</td>
                        <td className="leaderboard_name">{value.userName}</td>
                        <td className="leaderboard_points">{value.score}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                <p>No leaderboard data available.</p>
              )}
            </div>
            {/* <div id="profile">
              {leaderboardData.map((value, index) => (
                <div className="flex" key={index}>
                  <div className="leaderboard_index">
                    <h2>{index + 1}.</h2>
                  </div>
                  <div className="item">
                    <div className="info">
                      <h3 className="name text-dark">{value.userName}</h3>
                    </div>
                  </div>
                  <div className="item">
                    <h2>
                      <span>{value.score}</span>
                    </h2>
                  </div>
                </div>
              ))}
            </div> */}
          </div>
        </div>
      )}
    </>
  );
};

export default Leaderboard;

// <!DOCTYPE html>
// <html lang="en">
//   <head>
//     <link rel="preconnect" href="https://fonts.googleapis.com" />
//     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
//     <meta charset="UTF-8" />
//     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
//     <script src="https://unpkg.com/@phosphor-icons/web"></script>
//     <link
//       href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500&display=swap"
//       rel="stylesheet"
//     />
//   </head>
//   <body>
//     <main>
//       <div id="header">
//         <h1>Ranking</h1>
//         <button class="share">
//           <i class="ph ph-share-network"></i>
//         </button>
//       </div>
//       <div id="leaderboard">
//         <div class="ribbon"></div>
//         <table>
//           <tr>
//             <td class="number">1</td>
//             <td class="name">Lee Taeyong</td>
//             <td class="points">
//               258.244 <img class="gold-medal" src="https://github.com/malunaridev/Challenges-iCodeThis/blob/master/4-leaderboard/assets/gold-medal.png?raw=true" alt="gold medal"/>
//             </td>
//           </tr>
//           <tr>
//             <td class="number">2</td>
//             <td class="name">Mark Lee</td>
//             <td class="points">258.242</td>
//           </tr>
//           <tr>
//             <td class="number">3</td>
//             <td class="name">Xiao Dejun</td>
//             <td class="points">258.223</td>
//           </tr>
//           <tr>
//             <td class="number">4</td>
//             <td class="name">Qian Kun</td>
//             <td class="points">258.212</td>
//           </tr>
//           <tr>
//             <td class="number">5</td>
//             <td class="name">Johnny Suh</td>
//             <td class="points">258.208</td>
//           </tr>
//         </table>
//         <div id="buttons">
//           <button class="exit">Exit</button>
//           <button class="continue">Continue</button>
//         </div>
//       </div>
//     </main>
//   </body>
// </html>
