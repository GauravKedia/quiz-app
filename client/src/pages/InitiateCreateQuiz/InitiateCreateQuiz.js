import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { initiateCreateQuiz } from "../../controller/createQuiz/InitiateCreateQuiz";
import { useLoader } from "../../LoaderContext";
import Loader from "../loader/loader";
import "./InitiateCreateQuiz.css";

const InitiateCreateQuiz = () => {
  // const [quiz_id, setquiz_id] = useState("");
  const [quizName, setQuizName] = useState("");
  const [quizPin, setQuizPin] = useState("");
  const { universalLoader, setUniversalLoader } = useLoader();
  const navigate = useNavigate();

  useEffect(() => {
    setUniversalLoader(false);
  });

  const handleQuizNameChange = (event) => {
    setQuizName(event.target.value);
  };

  const handleQuizPinChange = (event) => {
    setQuizPin(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const res = await initiateCreateQuiz(quizName, quizPin);
      // console.log(res);
      if (res === "Initiate Successfull!") {
        navigate("/create_quiz/launch");
      } else if (res === "Quiz Already Present for this user") {
        window.alert(res);
        navigate("/create_quiz/launch");
      } else if(res === "Please Login Again"){
        window.alert(res);
        navigate("/auth/login");
      }
      else{
        window.alert(res);
      }
    } catch (error) {
      console.log("Error Occured :" + error);
    }
  };

  return (
    <>
      {universalLoader ? (
        <Loader />
      ) : (
        <div className="intitiate_create_quiz_container">
          <div className="initiate_create_quiz_inner_container">
            <h2>Quiz Detail</h2>
            <form
              onSubmit={handleSubmit}
              className="intitiate_create_quiz_form_container"
            >
              <div className="initiate_create_quiz_form">
                <label
                  htmlFor="user_name"
                  className="initiate_create_quiz_label"
                >
                  {" "}
                  Quiz Name
                </label>
                <input
                  // name="user_name"
                  placeholder="Enter Name for Quiz"
                  autoComplete="off"
                  id="user_name"
                  className="intitiate_create_quiz_input_container"
                  value={quizName}
                  onChange={handleQuizNameChange}
                />
              </div>
              <div className="initiate_create_quiz_form">
                <label
                  htmlFor="quiz_pin"
                  className="initiate_create_quiz_label"
                >
                  {" "}
                  Quiz Pin
                </label>
                <input
                  // name="quiz_pin"
                  placeholder="Enter PIN to access your quiz"
                  autoComplete="off"
                  id="quiz_pin"
                  className="intitiate_create_quiz_input_container"
                  value={quizPin}
                  onChange={handleQuizPinChange}
                />
              </div>
              <button
                className="intitiate_create_quiz_button_container"
                type="submit"
              >
                Submit
              </button>
            </form>
          </div>
        </div>
      )}
    </>
  );
};

export default InitiateCreateQuiz;
