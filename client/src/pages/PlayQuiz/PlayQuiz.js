import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import getQuestionData from "../../controller/PlayQuiz/getQuestionData";
import { checkAnswer } from "../../controller/PlayQuiz/checkAnswer";
import Loader from "../loader/loader";
import {useLoader} from "../../LoaderContext";
import "./PlayQuiz.css";
const PlayQuiz = () => {
  const backround_img = process.env.PUBLIC_URL + "/images/background.jpg";

  const navigate = useNavigate();

  const [quizName, setQuizName] = useState("");
  const [question, setQuestion] = useState("");
  const [options, setOptions] = useState([]);
  const [pts, setPts] = useState(0);
  const [countdown, setCountdown] = useState(0);
  const [userName, setUserName] = useState("");
  const [currentScore, setCurrentScore] = useState(0);
  const [currentQNo, setCurrentQNo] = useState(0);
  const [isCorrectAnswer, setIsCorrectAnswer] = useState(false);
  const [isWrongAnswer, setIsWrongAnswer] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const [disableSubmitAnswer, setDisableSubmitAnswer] = useState(false);
  const [afterSubmitAnswer, setAfterSubmitAnswer] = useState(null);
  const {universalLoader,setUniversalLoader} = useLoader();
  const [checkIfQuestionLoaded,setCheckIfQuestionLoaded] = useState(false);
    
  // const [timeToSolve, setTimeToSolve] = useState(countdown);

  useEffect(() => {
    // setPlayQuizLoading(true);
    const fetchData = async () => {
      // Calling Get Question Data
      getQuestionData(
        setQuizName,
        setQuestion,
        setOptions,
        setPts,
        setCountdown,
        setUserName,
        setCurrentScore,
        setCurrentQNo,
        setDisableSubmitAnswer,
        setAfterSubmitAnswer,
        setIsCorrectAnswer,
        setIsWrongAnswer,
        setUniversalLoader,
        navigate,
      );
    };

    fetchData();
  }, []);

  useEffect(()=>{
    if(checkIfQuestionLoaded){
      setUniversalLoader(false);
    }
    setCheckIfQuestionLoaded(true);
    // console.log("Resetting Loader to False");
  },[question])

  useEffect(() => {
    const timer = setInterval(() => {
      setCountdown((prev) => {
        if (prev === 0) {
          clearInterval(timer);
        }
        return prev > 0 ? prev - 1 : prev;
      });
    }, 1000);

    return () => clearInterval(timer);
  }, [countdown]);

  const SubmitAnswer = async () => {
    setUniversalLoader(true);
    setAfterSubmitAnswer(selectedOption);
    setDisableSubmitAnswer(true); 
    const checkAnswerResponse = await checkAnswer(
      selectedOption.optionValue,
      currentQNo,
      quizName
    );
    if (checkAnswerResponse === "Correct answer") {
      setIsCorrectAnswer(true);
    } else if (checkAnswerResponse === "Incorrect answer") {
      setIsWrongAnswer(true);
    }
    setUniversalLoader(false);
  };

  const handleOptionSelect = async (option) => {
    setSelectedOption(option);
  };

  const countdownColor = () => {
    const percent = (countdown / 10) * 100; // Convert countdown to percentage
  
    let color;
    if (percent <= 20) {
      // Use lipstick
      color = "#b3068c";
    } else if (percent <= 40) {
      // Use pompadour
      color = "#6a045c";
    } else if (percent <= 60) {
      // Use jaguar
      color = "#0f0421";
    } else if (percent <= 80) {
      // Use clairvoyant
      color = "#3e0440";
    } else {
      // Use light-orchid
      color = "#e4aecc";
    }
  
    return color;
  };


  return (
    <>
      {universalLoader ? (
        <Loader />
      ) : (
        <div className="play_quiz_container" style={{ backgroundImage: `url(${backround_img})` }}>
          <div className="play_quiz_navbar">
            <h1 className="play_quiz_title">Quizify</h1>
            <h1 className="play_quiz_test_name">{quizName}</h1>
            <div className="play_quiz_user_info">
              <h2>{`Username: ${userName}`}</h2>
              <h2>{`Current Score: ${currentScore}`}</h2>
            </div>
          </div>
          <div className="play_quiz_countdown">
            <div className="play_quiz_countdown_box" style={{ backgroundColor: countdownColor() }}>
              <p className="play_quiz_countdown_text">{countdown}</p>
            </div>
          </div>
          <div className="play_quiz_main_container">
            <div className="play_quiz_question_container">
              <div className="play_quiz_question_text">{question}</div>
            </div>
            <div className="play_quiz_options">
              {options.map((option, index) => (
                <div
                  key={index}
                  className={`play_quiz_option 
                  ${selectedOption === option ? "selected" : ""} 
                  `}
                  onClick={() => handleOptionSelect(option)}
                  >
                  {option.optionValue}
                </div>
              ))}
            </div>
            <div className="play_quiz_points">
              <h3>Points:</h3>
              <div className="play_quiz_points_box">{pts}</div>
            </div>
            <div className="play_quiz_actions">
              <button
                className={`play_quiz_submit_button ${disableSubmitAnswer===true ? "turn_submit_off" : ""}`}
                onClick={SubmitAnswer}
                disabled={disableSubmitAnswer}
                >
                Submit Answer
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default PlayQuiz;


// Code if On time correct answer to be shown
// ${
//   afterSubmitAnswer === option && isCorrectAnswer === true
//     ? "correct"
//     : ""
// }
// ${
//   afterSubmitAnswer === option && isWrongAnswer === true
//     ? "wrong"
//     : ""
// }