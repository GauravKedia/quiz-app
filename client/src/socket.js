import { io } from "socket.io-client";
import apiPaths from "./api";

const accessToken = localStorage.getItem("accessToken");
const refreshToken = localStorage.getItem("refreshToken");
// console.log("Socket Auth : "+ accessToken + " : "+refreshToken);
export const socket = io(apiPaths.socketUrl, {
  auth: {
    token: accessToken,
    refreshToken: refreshToken,
  },
  autoConnect: false 
});
// console.log("New Socket   Auth : "+ accessToken + " : "+refreshToken);

export const updateSocketHeaders = async () => {
  socket.auth = { 
    token: localStorage.getItem("accessToken"),
    refreshToken: localStorage.getItem("refreshToken"),
  };
  // console.log("Socket Updated");
  // console.log("New Session Token "+ localStorage.getItem("accessToken"));
};

