import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const deployQuiz = async (quizName, scheduleDate) => {
  try {
    const headers = createHeader();
    // console.log("Bye Bye Frontend");
    // console.log("deploy :" + apiPaths.loadQuiz);
    await axios.post(
      apiPaths.deployQuiz,
      { quizName, scheduleDate },
      { headers: headers }
    );
  } catch (error) {
    throw new Error(error);
  }
};

export { deployQuiz };
