import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const saveCreateQuiz = async (quizData) => {
  try {
    const headers = createHeader();
    const saveQuizResponse = await axios.post(
      apiPaths.saveCreateQuiz,
      { quizData },
      { headers: headers }
    );
    // console.log("Save Quiz Response: " + saveQuizResponse.data.message);
    return saveQuizResponse.data.message;
  } catch (error) {
    throw new Error(error);
  }
};

export { saveCreateQuiz };
