import axios from "axios";
import apiPaths from "../../api";
import {createHeader} from "../createHeader";
const createQuizStartLaunch = async () =>{
    
    try{
        const headers = createHeader();
        // console.log("api :" +apiPaths.createQuizStartLaunch);
        const createQuizStartLaunchResponse = await axios.post(apiPaths.createQuizStartLaunch,{},{headers:headers});
        // console.log("Hi  : "+createQuizStartLaunchResponse);
        return createQuizStartLaunchResponse;
    }
    catch(error){
        throw new Error(error.login_response.data.message);
    }
}

export {createQuizStartLaunch};