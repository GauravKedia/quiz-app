import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const createQuizLoadData = async (quizName) =>{
    try{
        const headers = createHeader();
        // console.log("api :" +apiPaths.createQuizStartLaunch);
        const createQuizStartLaunchResponse = await axios.post(apiPaths.createQuizLoadData,{quizName:quizName},{headers:headers});
        return createQuizStartLaunchResponse.data;
    }
    catch(error){
        throw new Error(error.login_response.data.message);
    }
}

export {createQuizLoadData};