import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const initiateCreateQuiz = async (quizName, quizPin) => {
  try {
    const headers = createHeader();
    const initiateCreateQuizResponse = await axios.post(
      apiPaths.initiateCreateQuiz,
      { quizName: quizName, quizPin: quizPin },
      { headers: headers }
    );
    return initiateCreateQuizResponse.data.message;
    
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export { initiateCreateQuiz };
