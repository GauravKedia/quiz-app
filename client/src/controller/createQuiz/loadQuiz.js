import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const loadQuiz = async (quizName, questionNumber) => {
  try {
    const headers = createHeader();
    const loadQuizResponse = await axios.post(
      apiPaths.loadQuiz,
      { quizName, questionNumber },
      { headers: headers }
    );
    // console.log("Load Quiz Response: " + loadQuizResponse);
    return loadQuizResponse.data;
  } catch (error) {
    throw new Error(error);
  }
};

export { loadQuiz };
