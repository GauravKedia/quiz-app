import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const deleteQuiz = async (quizName, questionNumber) => {
  try {
    const headers = createHeader();
    // console.log("Bye Bye Frontend");
    // console.log(apiPaths.deleteQuiz);
    const loadQuizResponse = await axios.post(
      apiPaths.deleteQuiz,
      { quizName: quizName, questionNumber: questionNumber },
      { headers: headers }
    );
    // console.log("Delete Quiz Response: " + loadQuizResponse);
    return loadQuizResponse.data;
  } catch (error) {
    throw new Error(error);
  }
};

export { deleteQuiz };
