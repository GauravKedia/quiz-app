import axios from 'axios';
import apiPaths from '../api';
const Login = async (userName, password) => {
  try {
    // console.log(userName + " ::: " +password);
    // console.log(apiPaths.login);
    const login_response = await axios.post(apiPaths.login, { userName:userName, password:password});
    localStorage.setItem('accessToken', login_response.data.accesstoken);
    localStorage.setItem('refreshToken', login_response.data.refreshtoken);
    if(login_response){
      // console.log(login_response);
      return login_response.data.message;
    }
  } catch (error) {         
    throw new Error(error.login_response.data.message);
  }
};  

const Register = async (firstName,lastName,userName,email, password) => {
    try {
      const register_response = await axios.post(apiPaths.register, { firstName:firstName,lastName:lastName,userName:userName,email:email, password:password });
      if(register_response.data.accesstoken && register_response.data.refreshtoken){
        localStorage.setItem('accessToken', register_response.data.accesstoken);
        localStorage.setItem('refreshToken', register_response.data.refreshtoken);
    }
    return register_response.data.message;
    } catch (error) {         
      throw new Error(error.register_response.data.message);
    }
  };
  
export { Login, Register };
    