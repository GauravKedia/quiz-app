const createHeader = () => {
  try {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");
    return {
      Authorization: `Bearer ${accessToken} ${refreshToken}`,
    };
  } catch (error) {
    console.log("Error Creating Header: " + error);
    return;
  }
};

export { createHeader };
