import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";

const checkAnswer = async (optionValue, currentQNo, quizName) => {
  try {
    const headers = createHeader();
    // console.log("Check Answer Response Sending Data to Backend : " + quizName +" : " + optionValue +" : " +currentQNo);
    // console.log("Check Answer Backend Path : " + apiPaths.checkAnswer);
    const checkAnswerResponse = await axios.post(
      apiPaths.checkAnswer,
      { quizName: quizName, optionValue: optionValue, currentQNo: currentQNo },
      { headers: headers }
    );
    if (checkAnswerResponse) {
      // console.log("Check Answer: " + checkAnswerResponse.data.message);
      return checkAnswerResponse.data.message;
    }
    return;
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export { checkAnswer };
