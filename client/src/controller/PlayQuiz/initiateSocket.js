import { socket } from "../../socket";

const initiateSocket = async (quizName, navigate) => {
  // console.log("Initiate Socket Received : " + quizName);

  socket.on("connect", () => {
    // console.log("Socket ID after Connect:", socket.id);
    try {
      // console.log("Going to Socket : check_game_started : quizName :" + quizName);
      // Emiting Check Game Started
      socket.emit("check_game_started", { quizName }, (checkGameStartedResponse) => {
        if (checkGameStartedResponse) {
          // console.log("Check Game Started Response : " + checkGameStartedResponse);
          if (checkGameStartedResponse === "Quiz Over") {
            navigate("/leaderboard");
          }
          else if(checkGameStartedResponse === "Sending Question!"){
              // console.log("Returned to CheckGameStarted : " + checkGameStartedResponse);
            try {
              // Emiting Reuquest Question
              navigate("/play_quiz");
              socket.emit("request_question", quizName, (requestQuestionResponse) => {
              if(requestQuestionResponse){
                window.alert(requestQuestionResponse);
              }
              });
            } catch (error) {
              console.log("Error in emit in request question : " + error);
            }
          }
          else if(checkGameStartedResponse === "Quiz Already Initiated"){ 
            // console.log("Else if of check game started response ")
            navigate("/play_quiz");
          }
          else{
            window.alert(checkGameStartedResponse);
          }
        }
      });
    } catch (error) {
      console.error("Error starting game:", error);
    }
  });

  socket.on("disconnect",()=>{
    // console.log("Socket Disconnected");
    socket.emit("on_disconnect", { quizName }, (response) => {
      console.log("Response after method_on_disconnect:", response);
      
    });
  })
};

export default initiateSocket;
