import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";

const initiatePlayQuiz = async (quizName, quizPin) => {
  try {
    const headers = createHeader();
    // console.log("Initiate Play Quiz going to backend: " + quizName + "  : " + quizPin);
    const initiatePlayQuizResponse = await axios.post(
      apiPaths.initiatePlayQuiz,
      { quizName: quizName, quizPin: quizPin },
      { headers: headers }
    );
    if (initiatePlayQuizResponse) {
      console.log("Initiate Playe QUIZ Recieved: " + initiatePlayQuizResponse.data.message);
      return initiatePlayQuizResponse.data.message;
    }
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export { initiatePlayQuiz };
