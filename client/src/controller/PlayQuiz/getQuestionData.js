import { socket } from "../../socket";
import playQuizLoadData from "./playQuizLoadData";
const getQuestionData = (
  setQuizName,
  setQuestion,
  setOptions,
  setPts,
  setCountdown,
  setUserName,
  setCurrentScore,
  setCurrentQNo,
  setDisableSubmitAnswer,
  setAfterSubmitAnswer,
  setIsCorrectAnswer,
  setIsWrongAnswer,
  setUniversalLoader,
  navigate,
) => {
  try {
    // console.log("Came to Get Question Data");
    socket.on("receive_question", async (questionResponse) => {
      setUniversalLoader(true);
      // console.log("Made Loader True");
      setDisableSubmitAnswer(false);
      setAfterSubmitAnswer(null);
      setIsCorrectAnswer(false);
      setIsWrongAnswer(false);

      if (questionResponse.message === "Question Sent!") {
        const { quizName, questionInfo } = questionResponse;

        await playQuizLoadData(quizName, setUserName, setCurrentScore);

        // console.log(quizName+"  :::: "+questionInfo.qNo);
        // console.log("Receive V: "+ questionInfo.qValue);
        // console.log("Receive O1: "+ questionInfo.options[0].optionNo+ " : " + questionInfo.options[0].optionValue);
        // console.log("Receive O2: "+ questionInfo.options[1].optionNo+ " : " + questionInfo.options[1].optionValue);
        // console.log("Receive O3: "+ questionInfo.options[2].optionNo+ " : " + questionInfo.options[2].optionValue);
        // console.log("Receive O4: "+ questionInfo.options[3].optionNo+ " : " + questionInfo.options[3].optionValue);
        // console.log("Receive P: "+ questionInfo.points);
        // console.log("Receive T: "+ questionInfo.timeOfQuestion);
        
        setCurrentQNo(questionInfo.qNo);
        setQuizName(quizName);
        setQuestion(questionInfo.qValue);
        setCountdown(questionInfo.timeOfQuestion);
        setPts(questionInfo.points);
        setOptions(
          questionInfo.options.map((option) => ({
            optionNo: option.optionNo,
            optionValue: option.optionValue,
          }))
        );

      } else  if(questionResponse.message === "Question Over!"){
        // console.log("Question Response from Backend : Question Over");
        socket.disconnect();
        navigate('/leaderboard');
      }
    });
  } catch (error) {
    console.log("Get Question Data: " + error);
  }
};
export default getQuestionData;
