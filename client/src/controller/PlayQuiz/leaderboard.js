import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";

const leaderboard = async (quizName) => {
  try {
    const headers = createHeader();
      // console.log("leaderboard sending : "+ quizName);
      const leaderboardResponse = await axios.post(
        apiPaths.leaderboard,
        { quizName: quizName},
        { headers: headers }
      );
      if(leaderboardResponse){
        // console.log("leaderboard Recieved: " + leaderboardResponse.data);
        return leaderboardResponse.data;
      }
      return null;
  } catch (error) {
    console.log("Controller: "+error.login_response.data.message);
  }
};

export { leaderboard };
