import axios from "axios";
import apiPaths from "../../api";
import { createHeader } from "../createHeader";
const playQuizLoadData = async (quizName, setUserName, setCurrentScore) => {
  try {
    const headers = createHeader();
    //   console.log("Play Quiz Load Data api :" + apiPaths.playQuizLoadData);
    const playQuizLoadDataResponse = await axios.post(
      apiPaths.playQuizLoadData,
      { quizName: quizName },
      { headers: headers }
    );
    if (
      playQuizLoadDataResponse &&
      playQuizLoadDataResponse.data.message === "Play Quiz Data Found!"
    ) {

      setUserName(playQuizLoadDataResponse.data.userName);
      setCurrentScore(playQuizLoadDataResponse.data.score);
    }
  } catch (error) {
    throw new Error(error.login_response.data.message);
  }
};

export default playQuizLoadData;
