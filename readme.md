# Quiz App 
### [Website Link](https://quiz-app-client-1jn8.onrender.com)

Welcome to the Quiz App project! This web application allows users to create and participate in quizzes. The project is divided into a React.js frontend and a Node.js backend using MongoDB as the database.

## Project Overview

The Quiz App is a web application that enables users to:

- Create quizzes
- Join existing quizzes
- Participate in quizzes
- View quiz results

The project is structured with a React.js frontend in the `client` folder and a Node.js backend in the `server` folder. For detailed documentation, check the `documentations` folder.

## Folder Structure

- `server`: Houses the Node.js backend code.
  - `Routes`: Router to redirect incoming requests to proper controllers.
  - `Models`: Schemas for the database. For more details about the database, check the `database.md` file.
  - `Middleware`: General Authentication and Socket Authentication Code.
  - `Controllers`: Files that contain main functions and interact with the database.
  - `Test`: Unit Testing Files.

- `client`: Holds the React.js frontend code with the following structure:
  - `public`: Public assets and HTML template.
  - `src`: React.js source code.
    - `Pages`: Individual pages or views.
      - `Landing Page`: Page where all users visit upon opening the website.
      - `Loader`: Loader Page which displays loading animation while loading anything.
      - `Auth`: Register & Login Page.
      - `Initiate Create Quiz`: Taking New Quiz Details from the user to create a quiz.
      - `Create Quiz`: Pages to Create a Quiz.
      - `Initiate Play Quiz`: Taking Quiz Details from the user to provide access to play the quiz.
      - `Play Quiz`: Page where users will play the quiz.
      - `Leaderboard`: Leaderboard Page after ending the quiz.
    - `Controllers`: All functions which will interact with the backend and provide functionality to the website.

## Start

1. Navigate to the `client` folder using the terminal:

```bash
cd client
```

2. Install npm packages and start

```bash
npm install
npm start
```

3. Navigate to the `server` folder using the terminal:

```bash
cd server
```

4. Install npm package and run server
```bash
npm install 
npm run server
```

